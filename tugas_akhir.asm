.MODEL SMALL
.CODE
org 100h
start:
        jmp mulai 
        
hp           db 13,10,'Masukan no hp anda : $'
gopay       db 13,10,'Masukan sandi akun GOPay anda : $'
lanjut       db 13,10,'TEKAN (Y) untuk melanjutkan : $' 
input_hp     db 13,?,13 dup(?)
input_gopay db 13,?,13 dup(?)

intro    db 13,10,'===================================================================='
         db 13,10,'======================     TELKOM X GOPAY     ======================'
         db 13,10,'===================================================================='
         db 13,10,'()','$'
              
daftar   db 13,10,'()'
         db 13,10,'=DAFTAR PAKET HARIAN:====================================================='
         db 13,10,'===================================================================='
         db 13,10,'=| NO |  INTERNET  | MASA AKTIF | TOTAL HARGA | METODE PEMBAYARAN |='    
         db 13,10,'=| 01 |    1 GB    |   3 HARI   |  Rp 19.500  |      GO-PAY       |='
         db 13,10,'=| 02 |    2 GB    |   3 HARI   |  Rp 27.500  |      GO-PAY       |=' 
         db 13,10,'=| 03 |    3 GB    |   1 HARI   |  Rp 16.500  |      GO-PAY       |='
         db 13,10,'=| 04 |    3 GB    |   3 HARI   |  Rp 32.500  |      GO-PAY       |=' 
         db 13,10,'=| 05 |    7 GB    |   3 HARI   |  Rp 45.500  |      GO-PAY       |='
         db 13,10,'=| 06 |   15 GB    |   3 HARI   |  Rp 58.500  |      GO-PAY       |='                       
         db 13,10,'====================================================================','$'

daftar2  db 13,10,'()'
         db 13,10,'=DAFTAR PAKET MINGGUAN:==================================================='
         db 13,10,'====================================================================' 
         db 13,10,'=| NO |  INTERNET  | MASA AKTIF | TOTAL HARGA | METODE PEMBAYARAN |='   
         db 13,10,'=| 07 |    1 GB    |   7 HARI   |  Rp 34.500  |      GO-PAY       |='
         db 13,10,'=| 08 |    2 GB    |   7 HARI   |  Rp 43.500  |      GO-PAY       |=' 
         db 13,10,'=| 09 |    3 GB    |   7 HARI   |  Rp 51.500  |      GO-PAY       |='
         db 13,10,'=| 10 |    5 GB    |   7 HARI   |  Rp 58.500  |      GO-PAY       |=' 
         db 13,10,'=| 11 |   15 GB    |   7 HARI   |  Rp 75.500  |      GO-PAY       |='                        
         db 13,10,'====================================================================','$'   
         
daftar3  db 13,10,'()'
         db 13,10,'=DAFTAR PAKET BULANAN:===================================================='  
         db 13,10,'===================================================================='
         db 13,10,'=| NO |  INTERNET  | MASA AKTIF | TOTAL HARGA | METODE PEMBAYARAN |='   
         db 13,10,'=| 12 |    4 GB    |  30 HARI   |  Rp 59.000  |      GO-PAY       |='
         db 13,10,'=| 13 |   10 GB    |  30 HARI   |  Rp 112.000 |      GO-PAY       |=' 
         db 13,10,'=| 14 |   17 GB    |  30 HARI   |  Rp 148.000 |      GO-PAY       |='                        
         db 13,10,'====================================================================','$'        

error           db 13,10,'INPUT YANG ANDA MASUKKAN SALAH! $'
input_daftar    db 13,10,'masukkan NO daftar yang anda pilih cnth=(01/11) : $'
succes          db 13,10,'SELAMAT ANDA BERHASIL $'
                                        
mulai:  
    mov ah,09h
    mov dx,offset intro
    int 21h    
    
    mov ah,09h
    lea dx,hp
    int 21h
    mov ah,0ah
    lea dx,input_hp
    int 21h
    push dx  
    
    mov ah,09h
    lea dx,gopay
    int 21h
    mov ah,0ah
    lea dx,input_gopay
    int 21h
    push dx
        
    mov ah,09h
    mov dx,offset daftar
    int 21h
    mov ah,09h
    lea dx,lanjut
    int 21h    
    mov ah,01h
    int 21h
    cmp al,'y'
    je  page2
    jne error_msg

    
page2:   
    mov ah,09h
    mov dx,offset daftar2
    int 21h
    mov ah,09h
    mov dx,offset lanjut
    int 21h   
    mov ah,01h
    int 21h
    cmp al,'y'
    je  page3
    jne error_msg  

page3:   
    mov ah,09h
    mov dx,offset daftar3
    int 21h
    mov ah,09h
    mov dx,offset lanjut
    int 21h   
    mov ah,01h
    int 21h
    cmp al,'y'
    je  proses
    jne error_msg                                                 
    
;-----------------------------------------------------------------
        
error_msg:
    mov ah,09h
    mov dx,offset error
    int 21h
    
    int 20h

;-----------------------------------------------------------------

proses:
    mov ah,09h
    mov dx,offset input_daftar
    int 21h
    
    mov ah,1
    int 21h
    mov bh,al
    mov ah,1
    int 21h
    mov bl,al
    
    cmp bh,'0'
    cmp bl,'1'
    je hasil1 
    
    cmp bh,'0'
    cmp bl,'2'
    je hasil2 
    
    cmp bh,'0'
    cmp bl,'3'
    je hasil3 
    
    cmp bh,'0'
    cmp bl,'4'
    je hasil4 
    
    cmp bh,'0'
    cmp bl,'5'
    je hasil5 
    
    cmp bh,'0'
    cmp bl,'6'
    je hasil6 
    
    cmp bh,'0'
    cmp bl,'7'
    je hasil7 
    
    cmp bh,'0'
    cmp bl,'8'
    je hasil8 
    
    cmp bh,'0'
    cmp bl,'9'
    je hasil9 
    
    cmp bh,'1'
    cmp bl,'0'
    je hasil10 
    
    cmp bh,'1'
    cmp bl,'1'
    je hasil11
    
    cmp bh,'1'
    cmp bl,'2'
    je hasil12
    
    cmp bh,'1'
    cmp bl,'3'
    je hasil13
    
    cmp bh,'1'
    cmp bl,'4'
    je hasil14 
    
    jne error_msg 
    
;-----------------------------------------------------------------
    
hasil1:
    mov ah,09h
    lea dx,teks1
    int 21h
    int 20h  
    
hasil2:
    mov ah,09h
    lea dx,teks2
    int 21h
    int 20h
    
hasil3:
    mov ah,09h
    lea dx,teks3
    int 21h
    int 20h
    
hasil4:
    mov ah,09h
    lea dx,teks4
    int 21h
    int 20h
    
hasil5:
    mov ah,09h
    lea dx,teks5
    int 21h
    int 20h
    
hasil6:
    mov ah,09h
    lea dx,teks6
    int 21h
    int 20h
    
hasil7:
    mov ah,09h
    lea dx,teks7
    int 21h
    int 20h
    
hasil8:
    mov ah,09h
    lea dx,teks8
    int 21h
    int 20h
    
hasil9:
    mov ah,09h
    lea dx,teks9
    int 21h
    int 20h
    
hasil10:
    mov ah,09h
    lea dx,teks10
    int 21h
    int 20h
  
hasil11:
    mov ah,09h
    lea dx,teks11
    int 21h
    int 20h
    
hasil12:
    mov ah,09h
    lea dx,teks12
    int 21h
    int 20h
    
hasil13:
    mov ah,09h
    lea dx,teks13
    int 21h
    int 20h
    
hasil14:
    mov ah,09h
    lea dx,teks14
    int 21h
    int 20h   

;-----------------------------------------------------------------
    
teks1   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 1 GB' 
        db 13,10,'Harian selama 3 hari RP 19.500'
        db 13,10,'telah aktif, berlaku s/d 3 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'
         
teks2   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 2 GB' 
        db 13,10,'Harian selama 3 hari RP 27.500'
        db 13,10,'telah aktif, berlaku s/d 3 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'  
        
teks3   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 3 GB' 
        db 13,10,'Harian selama 1 hari RP 16.500'
        db 13,10,'telah aktif, berlaku s/d 1 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$' 
     
teks4   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 3 GB' 
        db 13,10,'Harian selama 3 hari RP 32.500'
        db 13,10,'telah aktif, berlaku s/d 3 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'  
        
teks5   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 7 GB' 
        db 13,10,'Harian selama 3 hari RP 45.500'
        db 13,10,'telah aktif, berlaku s/d 3 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'  
        
teks6   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 15 GB' 
        db 13,10,'Harian selama 3 hari RP 58.500'
        db 13,10,'telah aktif, berlaku s/d 3 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'   
        
teks7   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 1 GB' 
        db 13,10,'Mingguan selama 7 hari RP 34.500'
        db 13,10,'telah aktif, berlaku s/d 7 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'
        
teks8   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 2 GB' 
        db 13,10,'Mingguan selama 7 hari RP 43.500'
        db 13,10,'telah aktif, berlaku s/d 7 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'   
        
teks9   db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 3 GB' 
        db 13,10,'Mingguan selama 7 hari RP 51.500'
        db 13,10,'telah aktif, berlaku s/d 7 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$' 
        
teks10  db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 5 GB' 
        db 13,10,'Mingguan selama 7 hari RP 58.500'
        db 13,10,'telah aktif, berlaku s/d 7 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'
        
teks11  db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 15 GB' 
        db 13,10,'Mingguan selama 7 hari RP 75.500'
        db 13,10,'telah aktif, berlaku s/d 7 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'   
        
teks12  db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 4 GB' 
        db 13,10,'Bulanan selama 30 hari RP 59.000'
        db 13,10,'telah aktif, berlaku s/d 30 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$' 
        
teks13  db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 10 GB' 
        db 13,10,'Bulanan selama 30 hari RP 112.000'
        db 13,10,'telah aktif, berlaku s/d 30 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'
        
teks14  db 13,10,'================================='
        db 13,10,'Selamat, Paket Internet 17 GB' 
        db 13,10,'Bulanan selama 30 hari RP 148.000'
        db 13,10,'telah aktif, berlaku s/d 30 hari'
        db 13,10,'kedepan pkl.23:59 WIB.'
        db 13,10,'Terima Kasih telah berlangganan<3','$'    
        
end start